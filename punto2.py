from pyfirmata import Arduino, util
from tkinter import *
from tkinter import messagebox
from PIL import Image , ImageTk  #libreria manejo imagenes
from time import sleep

window_width=300
window_height=1080
canvas_width =  1900
canvas_height = 980
sizex_circ=60
sizey_circ=60
width = 100
height = 70


placa = Arduino ('COM6')
it = util.Iterator(placa)
#inicio el iteratodr
it.start()
led = placa.get_pin('d:9:p') 
led1 = placa.get_pin('d:10:p') 
ventana = Tk()
ventana.state('zoomed') #iniciar la pantalla maximizada
ventana.configure(bg = 'white')
ventana.title("Repaso")
lienzo = Canvas(ventana, width=canvas_width, height=canvas_height, bg="lavender")
lienzo.place(x =10,y = 10)
texto = Label(lienzo, text="Ejercicio repaso parcial", bg='maroon4', font=("Berlin Sans FB", 20), fg="white")
texto.place(x=850, y=20)
img = Image.open("C:/Users/Camilo/Downloads/logousa.png") # abro la imagen
img = img.resize((width,height)) #se cambia el tamaño
imagen =  ImageTk.PhotoImage(img)  #almaceno la imagen en un objeto
Label(lienzo,image=imagen).place(x = window_width-200 ,y = 10) #coloco la imagen en un objeto label
dibujo_led1=lienzo.create_oval(490,165,490+sizex_circ,165+sizey_circ,fill="white")
dibujo_led2=lienzo.create_oval(490,320,490+sizex_circ,320+sizey_circ,fill="white")

def intensidad(dato_lum):
    dato_lum=int(dato_lum)
    if(dato_lum>80):
        lienzo.itemconfig(dibujo_led1, fill="blue4")
    if(dato_lum>60 and dato_lum<=80):
        lienzo.itemconfig(dibujo_led1, fill="blue2")
    if(dato_lum>40 and dato_lum<=60):
        lienzo.itemconfig(dibujo_led1, fill="DeepSkyBlue2")
    if(dato_lum>20 and dato_lum<=40):
        lienzo.itemconfig(dibujo_led1, fill="SkyBlue1")
    if (dato_lum<=20) :
        lienzo.itemconfig(dibujo_led1, fill="white")
    led.write(float(dato_lum)/100)
    
def intensidad2(dato_lum):
    dato_lum=int(dato_lum)
    if(dato_lum>80):
        lienzo.itemconfig(dibujo_led2, fill="blue4")
    if(dato_lum>60 and dato_lum<=80):
        lienzo.itemconfig(dibujo_led2, fill="blue2")
    if(dato_lum>40 and dato_lum<=60):
        lienzo.itemconfig(dibujo_led2, fill="DeepSkyBlue2")
    if(dato_lum>20 and dato_lum<=40):
        lienzo.itemconfig(dibujo_led2, fill="SkyBlue1")
    if (dato_lum<=20) :
        lienzo.itemconfig(dibujo_led2, fill="white")
    led1.write(float(dato_lum)/100)

lum = Scale(ventana, 
            command=intensidad, 
            from_= 0, 
            to = 100, 
            orient = HORIZONTAL , 
            length =200 , 
            label = 'Regular Intensidad',
            bg = 'purple4',
            font=('Helvetica',12 ),
            fg="red"

            )




lum2 = Scale(ventana, 
            command=intensidad2, 
            from_= 0, 
            to = 100, 
            orient = HORIZONTAL , 
            length =200 , 
            label = 'Regular Intensidad',
            bg = 'purple4',
            font=('Helvetica',12 ),
            fg="red"

            )
lum.place(x=425 ,y=70 )
lum2.place(x=425 ,y=250 )
ventana.mainloop()